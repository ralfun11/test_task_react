import React, { useContext } from "react";
import Context from '../../context'

const TodoItem = (props) => {
    const {removeTodo, toggleTodo} = useContext(Context)
    return (
        <div className={props.className}>
            <div onClick={()=>{toggleTodo(props.todo.id)}}>
                <input type="radio" readOnly checked={props.todo.completed}></input>
                <span>{props.todo.title}</span>
            </div>
            <button onClick={() => {removeTodo(props.todo.id)}} onMouseOver={(event) => {event.target.style.color = '#af5b5e'}} onMouseOut={(event) => {event.target.style.color = 'rgb(99,99,99)'}}>X</button>
        </div>
    )
}

export default TodoItem