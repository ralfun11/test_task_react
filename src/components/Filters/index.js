import React, { useContext } from 'react'

import Context from '../../context'

const Filters = (props) => {

    const { toggleIncompleted, toggleCompleted, isCompletedVisible, isIncompletedVisible } = useContext(Context)

    return(
        <div className={props.className}>
            <div onClick={() => {toggleCompleted()}} style={isCompletedVisible ? {'backgroundColor':'rgb(99,99,99)','color':'white'} : {}}>
                {/* <input name="completed" type="checkbox" checked={isCompletedVisible}></input> */}
                <span>Show Completed</span>
            </div>
            <div onClick={() => {toggleIncompleted()}} style={isIncompletedVisible ? {'backgroundColor':'rgb(99,99,99)','color':'white'} : {}}>
                {/* <input name="incompleted" type="checkbox" checked={isIncompletedVisible}></input> */}
                <span>Show Incompleted</span>
            </div>
        </div>
    )
}

export default Filters