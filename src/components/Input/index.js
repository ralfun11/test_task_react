import React, { useContext, useState } from 'react'
import Context from '../../context'

const Input = (props) => {
    const { addTodo } = useContext(Context)
    const [value, setValue] = useState('')
    return(
        <form className={props.className} onSubmit={()=>{addTodo(value)}}>
            <input autoFocus type='text' placeholder='Is there something you need to do today?' onChange={(event) => {
                setValue(event.target.value)}}>
            </input>
        </form>
    )
}

export default Input