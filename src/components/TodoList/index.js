import React,{ useContext } from 'react'
import Context from '../../context'
import TodoItem from '../TodoItem'
import styled from 'styled-components'

const StyledTodoItem = styled(TodoItem)`
    /* margin: 1vh; */
    padding: 2.2vh;
    display: flex;
    justify-content: space-between;
    align-items: center;

    background-color: white;
    /* border-bottom: #D1CDC7 2px solid; */
    border-top: #D1CDC7 2px solid;
    font-size: 2.5vh;
    color: rgb(99,99,99);

    & > div {
        display: flex;
        align-items: center;
        word-break: break-all
    }

    & > button {
    border: none;
	margin: auto 0;
	font-size: 2.5vh;
    background: white;
    color: rgb(99,99,99)
    }

    & > div input[type="radio"] {
        height: 2.5vh;
        width: 2.5vh;
        margin-right: 1.5vh
    }
`

const TodoList = (props) => {
    const { isCompletedVisible, isIncompletedVisible } = useContext(Context)
    
    return props.todos.map((todo) => {
        if ((isCompletedVisible && todo.completed) || (isIncompletedVisible && !todo.completed)){
            return <StyledTodoItem todo={todo} key={todo.id}></StyledTodoItem>
        }
        return <></>
    })
}

export default TodoList