import React, { useState } from 'react'

import TodoList from './components/TodoList'
import Input from './components/Input'
import Filters from './components/Filters'
import Context from './context'
import styled from 'styled-components';

import './App.css';


const StyledInput = styled(Input)`
display: flex;
flex-direction: column;
font-size: 3vh;
border-radius: 1vh;
background-color: white;

& > input {
  height: 6vh;
  margin: 0;
  font-size: 3.5vh; 
  padding-left: 1vh
}
`
const StyledTodoList = styled(TodoList)`
list-style: none;
margin: 1vh;
padding: 0 1vh;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`
const StyledFilters = styled(Filters)`
  color: rgb(99,99,99);
  display: flex;
  justify-content: space-evenly;
  /* border: 2px black solid; */
  margin: 1vh;
  font-size: 2.2vh;

  & > div {
    padding: 1vh;
    border: 1px solid rgb(99,99,99);
  }
`

const App = () => {

  const [todos, setTodos] = useState(JSON.parse(window.localStorage.getItem('todos')))
  const [isCompletedVisible, setIsCompletedVisible] = useState(true)
  const [isIncompletedVisible, setIsIncompletedVisible] = useState(true)

  const toggleCompleted = () => {
    setIsCompletedVisible(!isCompletedVisible)
  }

  const toggleIncompleted = () => {
    setIsIncompletedVisible(!isIncompletedVisible)
  }

  const addTodo = (title) => {
    const date = new Date()
    const newTodos = [...todos, {
      id: `${date.getMonth()}-${date.getDay()}-${date.getTime()}`,
      title,
      completed: false
    }]
    window.localStorage.setItem('todos', JSON.stringify(newTodos))
    setTodos(newTodos)
  }

  const removeTodo = (id) => {
    const newTodos = todos.filter(todo => todo.id !== id)
    setTodos(newTodos)
    window.localStorage.setItem('todos', JSON.stringify(newTodos))
  }

  const toggleTodo = (id) => {
    const newTodos = todos.map(todo => {
      if (todo.id === id) todo.completed = !todo.completed;
      return todo
    })
    window.localStorage.setItem('todos', JSON.stringify(newTodos))
    setTodos(newTodos)
  }
    return (
      <div className="App">
        <Context.Provider value={{addTodo, removeTodo, toggleTodo, isCompletedVisible, isIncompletedVisible, toggleCompleted, toggleIncompleted}}>
          <StyledInput></StyledInput>
          {(Object.keys(todos).length !== 0) ? <StyledFilters></StyledFilters> : <></>}
          <StyledTodoList todos={todos}></StyledTodoList>
        </Context.Provider>
      </div>
    )
}

export default App;
